package planilha;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import model.Cidade;
import model.Distancia;

public class FerramentaPlanilha {

	public static List<Cidade> lerPlanilha() throws Exception
	{
		 FileInputStream arquivo = new FileInputStream(new File("CAIXEIRO_VIAJANTE.xls"));
		 HSSFWorkbook workbook = new HSSFWorkbook(arquivo);
		 HSSFSheet sheetAlunos = workbook.getSheetAt(0);
		 Iterator<Row> rowIterator = sheetAlunos.iterator();

		 List<Cidade> cidades = null;
		 
		 while (rowIterator.hasNext()) 
		 {
			 Row linha = rowIterator.next();
			 
			 if(linha.getRowNum() == 0)
			 {
				 cidades = retornaListaCidades(linha);
			 }
			 else 
			 {
				 int iDist = 0;
				 Iterator<Cell> ci = linha.cellIterator();
				 Integer codigoCidadeDist = 0;
				 while(ci.hasNext())
				 {
					 Cell c = ci.next();
					 
					 if(c.getColumnIndex() == 0)
					 {
						 codigoCidadeDist = (int) c.getNumericCellValue();
					 }
					 else 
					 {
						 Integer valorDistancia = (int) c.getNumericCellValue();
						 cidades.get(iDist++).getDistanciasDasOutrasCidades().add(new Distancia(codigoCidadeDist, valorDistancia));
					 }
				 }
			}
		 }
		 
		 return cidades;
	}
	
	public static List<Cidade> retornaListaCidades(Row linha)
	{
		List<Cidade> listaCidades = new ArrayList<>();
		
		Iterator<Cell> colunas = linha.cellIterator();
		 
		while(colunas.hasNext())
		{
			Cell cell = colunas.next();

			if(cell.getColumnIndex() != 0)
			{
				listaCidades.add(new Cidade((int) cell.getNumericCellValue()));
			}
		}
		
		return listaCidades;
	}

}
