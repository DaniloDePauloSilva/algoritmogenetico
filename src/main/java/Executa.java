import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import model.Cidade;
import model.Cromossomo;
import model.Distancia;
import planilha.FerramentaPlanilha;

public class Executa {
	
	private static final int POPULACAO = 50;
	private static final int NUMERO_DE_GERAÇOES = 1000;
	
	private static final double taxa_permutacao = 0.8;
	private static final double taxa_mutacao = 0.2;
	
	public static void main(String[] args) throws Exception {
		
		List<Cidade> cidades = FerramentaPlanilha.lerPlanilha();
	
		for(Cidade c : cidades)
		{
			System.out.println("\nCIDADE: " + c.getCodigoCidade());
			
			System.out.println("DISTANCIAS: ");
			for(Distancia d : c.getDistanciasDasOutrasCidades())
			{
				System.out.println("codigo: " + d.getCodigoCidade() + " | DISTANCIA: " + d.getDistancia());
			}
		}
		
		List<Cromossomo> cromossomos = retornaCromossomos(cidades);
		
		logCromossomos(cromossomos);
		
		int cont = 0;
		
		while(cont < NUMERO_DE_GERAÇOES)
		{	
			List<Cromossomo> filhos = new ArrayList<>();
			
			for(int i = 0; i < (POPULACAO * taxa_permutacao); i++)
			{
				int indice1 = (int)(Math.random() * cromossomos.size());
				
				int indice2 = (int)(Math.random() * cromossomos.size());
				
				while(indice1 == indice2)
				{
					indice2 = (int)(Math.random() * cromossomos.size());
				}
				
				Cromossomo filho = realizarCruzamento(cromossomos.get(indice1), cromossomos.get(indice2));
				
				double vaMutacao = Math.random();
				
				if(vaMutacao < taxa_mutacao)
				{
					System.out.println("MUTACAO ANTES: ");
					logCromossomo(filho.getGenes());
					mutacao(filho);
					System.out.println("MUTACAO DEPOIS: ");
					logCromossomo(filho.getGenes());
				}
				
				filhos.add(filho);
			}
			
			cromossomos.addAll(filhos);
			
			Collections.sort(cromossomos);
			
			while(cromossomos.size() > POPULACAO)
			{
				cromossomos.remove(cromossomos.size() - 1);
			}
			
			System.out.println("FINAL CICLO: ");
			logCromossomos(cromossomos);
			
			cont++;
		
		}
		
		System.out.println("--------------------------------------------------------------");
		System.out.println("FIM PROCESSAMENTO:");
		System.out.println("\n\n\n");
		System.out.println("Melhor resultado encontrado: \n\n");
		logCromossomo(cromossomos.get(0));
		System.out.println("\n\n--------------------------------------------------------------");
	}
	
	public static void logCromossomo(Cromossomo crom) 
	{
		StringBuilder str = new StringBuilder();
		
		for(Cidade gene : crom.getGenes())
		{
			str.append(gene.getCodigoCidade() + " ");
		}
		
		System.out.println("CROMOSSOMO:" + str.toString() + " - CUSTO: " + crom.getCusto());
		
	}
	
	public static void logCromossomos(List<Cromossomo> cromossomos) 
	{
		System.out.println("\nCROMOSSOMOS: \n");
		
		for(Cromossomo crom : cromossomos) 
		{
			StringBuilder str = new StringBuilder();
	
			for(Cidade gene : crom.getGenes())
			{
				str.append(gene.getCodigoCidade() + " ");
			}
			
			System.out.println("CROMOSSOMO:" + str.toString() + " - CUSTO: " + crom.getCusto());
		}
	}
	
	// public static void logCromossomo(List<Cidade>)
	
	public static Cromossomo realizarCruzamento(Cromossomo c1, Cromossomo c2) 
	{
		Cromossomo csaida = new Cromossomo();
		
		csaida.setGenes(crossover(c1, c2));
		
		return csaida;
	}
	
	public static List<Cidade> crossover (Cromossomo c1, Cromossomo c2)
	{
		List<Cidade> aux1 = copiarGenes(c1.getGenes());
		List<Cidade> aux2 = copiarGenes(c2.getGenes());
		
		Cidade g1 = aux1.remove(0);
		Cidade g2 = aux2.remove(0);
		
				
		aux1.add(0, g2);
		aux2.add(0, g1);
	
		int indiceTroca = realizarAjusteCrossover(aux1, aux2, -1);
		System.out.println("===============================================");
		System.out.println("REALIZOU AJUSTE: indice " + indiceTroca);
		logCromossomo(aux1);
		logCromossomo(aux2);
		System.out.println("===============================================");
	
		while(indiceTroca > 0)
		{
			indiceTroca = realizarAjusteCrossover(aux1, aux2, indiceTroca);
			System.out.println("===============================================");
			System.out.println("REALIZOU AJUSTE: indice " + indiceTroca);
			logCromossomo(aux1);
			logCromossomo(aux2);
			System.out.println("===============================================");
		}
	
		logCromossomo(aux1);
		
		return aux1;
		
	}
	
	
	
	public static void logCromossomo(List<Cidade> c) 
	{
		System.out.println("CROMOSSOMO: ");
		
		for(int i = 0; i < c.size(); i++)
		{
			System.out.print(c.get(i).getCodigoCidade() + " ");
		}
		
		System.out.println("");
	}
	
	public static int realizarAjusteCrossover(List<Cidade> listaAlvo, List<Cidade> listaComparacao, int ultimaTroca)
	{
		for(int i = 0; i < listaAlvo.size(); i++)
		{
			Cidade c = listaAlvo.get(i);
			
			for(int j = 0; j < listaAlvo.size(); j++)
			{
				if(i != j && j != ultimaTroca)
				{
					Cidade c1 = listaAlvo.get(j);
					
					if(c1.getCodigoCidade() == c.getCodigoCidade())
					{
						Cidade cTroca1 = listaAlvo.remove(j);
						Cidade cTroca2 = listaComparacao.remove(j);
						
						listaComparacao.add(j, cTroca1);
						listaAlvo.add(j, cTroca2);
						
						return j;
					}
				}
			}
		}
		
		return -1;
		
	}
	
	public static List<Cidade> copiarGenes(List<Cidade> c1) 
	{
	    List<Cidade> c2 = new ArrayList<Cidade>(c1.size());
	    
	    for (Cidade item : c1)
	    	c2.add((Cidade) item.clone());
	    
	    
	    return c2;
	}
	
	public static void mutacao(Cromossomo cromossomo) 
	{
		int indice1 = (int)(Math.random() * (cromossomo.getGenes().size() - 1));
		int indice2 = (int)(Math.random() * (cromossomo.getGenes().size() - 1));
		
		while(indice1 == indice2)
		{
			indice2 = (int)(Math.random() * (cromossomo.getGenes().size() - 1));
		}
		
		Cidade c1 = cromossomo.getGenes().remove(indice1);
		Cidade c2 = cromossomo.getGenes().remove(indice2);
		
		cromossomo.getGenes().add(indice1, c2);
		cromossomo.getGenes().add(indice2, c1);
		
	}
	
	public static List<Cromossomo> retornaCromossomos(List<Cidade> listaCidades){
		
		List<Cromossomo> listaCromossomos = new ArrayList<>();
		
		while(listaCromossomos.size() < POPULACAO) 
		{
			Cromossomo cromossomo = new Cromossomo();
			
			for(int i = 0; i < listaCidades.size(); i++)
			{
				cromossomo.getGenes().add(sorteiaCidadeSemRepetir(cromossomo.getGenes(), listaCidades));
			}
			
			listaCromossomos.add(cromossomo);
		}
		
		Collections.sort(listaCromossomos);
		
		return listaCromossomos; 
	}
	
	public static Cidade sorteiaCidadeSemRepetir(List<Cidade> listaTarget, List<Cidade> listaSource)
	{
		int indice = (int) (Math.random() * listaSource.size());
		Cidade c = listaSource.get(indice);
		
		while(listaTarget.contains(c)) 
		{
			indice = (int) (Math.random() * listaSource.size());
			c = listaSource.get(indice);
		}
		
		return c;
		
	}
}
