package model;

public class Distancia {
	
	public Distancia(Integer codigoCidade, Integer distancia)
	{
		setCodigoCidade(codigoCidade);
		setDistancia(distancia);
	}
	
	private Integer codigoCidade;
	private Integer distancia;

	public Integer getDistancia() {
		return distancia;
	}

	public void setDistancia(Integer distancia) {
		this.distancia = distancia;
	}

	public Integer getCodigoCidade() {
		return codigoCidade;
	}

	public void setCodigoCidade(Integer codigoCidade) {
		this.codigoCidade = codigoCidade;
	}

}
