package model;

import java.util.ArrayList;
import java.util.List;

public class Cromossomo implements Comparable<Cromossomo>{
	
	private Integer id;
	private List<Cidade> genes;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List<Cidade> getGenes() {
		
		if(genes == null)
			genes = new ArrayList<>();
		
		return genes;
	}
	public void setGenes(List<Cidade> genes) {
		this.genes = genes;
	}
	
	public Double getCusto() 
	{
		Double distanciaPercorrida = 0.0;
		
		for(int i = 1; i < getGenes().size(); i++)
		{
			Cidade atual = getGenes().get(i - 1);
			Cidade proxima = getGenes().get(i);
			
			Double distanciaAteAProxima = distanciaDaCidade(proxima.getCodigoCidade(), atual.getDistanciasDasOutrasCidades()); 
			
			distanciaPercorrida += distanciaAteAProxima;
		}
		
		return distanciaPercorrida;
	}
	
	public Double distanciaDaCidade(Integer codigoCidade, List<Distancia> distancias) 
	{
		for(Distancia d : distancias)
		{
			if(d.getCodigoCidade().equals(codigoCidade))
				return d.getDistancia().doubleValue();
		}
		
		throw new RuntimeException();
	}
	@Override
	public int compareTo(Cromossomo arg0) {
	
		if(this.getCusto() > arg0.getCusto())
		{
			return 1;
		}
		
		if(this.getCusto() < arg0.getCusto())
		{
			return -1;
		}
		
		return 0;
	}
}
