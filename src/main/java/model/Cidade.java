package model;

import java.util.ArrayList;
import java.util.List;

public class Cidade {
	
	public Cidade() {}
	
	public Cidade(Integer codigoCidade)
	{
		setCodigoCidade(codigoCidade);
	}
	
	private Integer codigoCidade;
	
	private List<Distancia> distanciasDasOutrasCidades;

	public Integer getCodigoCidade() {
		return codigoCidade;
	}

	public void setCodigoCidade(Integer codigoCidade) {
		this.codigoCidade = codigoCidade;
	}

	public List<Distancia> getDistanciasDasOutrasCidades() {
		
		if(distanciasDasOutrasCidades == null)
			distanciasDasOutrasCidades = new ArrayList<>();
		
		return distanciasDasOutrasCidades;
	}

	public void setDistanciasDasOutrasCidades(List<Distancia> distanciasDasOutrasCidades) {
		this.distanciasDasOutrasCidades = distanciasDasOutrasCidades;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj instanceof Cidade)
		{
			Cidade cidObj = (Cidade) obj;
			
			return cidObj.getCodigoCidade().equals(this.getCodigoCidade());
		}
		
		return false;
	}
	
	
	public Cidade clone() 
	{
		Cidade c = new Cidade();
		
		c.setCodigoCidade(this.getCodigoCidade());
		
		for(Distancia d : this.getDistanciasDasOutrasCidades())
		{
			c.getDistanciasDasOutrasCidades().add(new Distancia(d.getCodigoCidade(), d.getDistancia()));
		}
		
		return c;
	}
	
}
